package facci.pm.baquejorge.myapplication;

import com.orm.SugarRecord;

public class SistemaOperativo extends SugarRecord<SistemaOperativo> {

    private String Android;
    private String Version;
    private String IDTelefono;


    public SistemaOperativo(String android, String version, String IDTelefono) {
        this.Android = android;
        this.Version = version;
        this.IDTelefono = IDTelefono;
    }

    public SistemaOperativo() {
    }

    public String getAndroid() {
        return Android;
    }

    public void setAndroid(String android) {
        Android = android;
    }

    public String getVersion() { return Version;}

    public void setVersion(String version) {Version = version;}


    public String getIDTelefono() {
        return IDTelefono;
    }

    public void setIDTelefono(String IDTelefono) {
        this.IDTelefono = IDTelefono;
    }
}
