package facci.pm.baquejorge.myapplication;
import com.orm.SugarRecord;

public class Telefono extends SugarRecord<Telefono> {

    //CAMPOS
    String Imei;
    String Gama;
    String Marca;
    String Modelo;
    String CompañiaTelefonica;

    //CONSTRUCTOR
    public Telefono(String imei, String gama, String marca, String modelo,String compañiatelefonica) {
        this.Imei = imei;
        Gama = gama;
        Marca = marca;
        Modelo = modelo;
        CompañiaTelefonica = compañiatelefonica;
    }

    //SOBRECARGA DEL CONSTRUCTOR
    public Telefono() {
    }

    //ENCAPSULACIÓN


    public String getImei() {
        return Imei;
    }

    public void setImei(String imei) {
        Imei = imei;
    }

    public String getGama() {
        return Gama;
    }

    public void setGama(String gama) {
        Gama = gama;
    }

    public String getMarca() {
        return Marca;
    }

    public void setMarca(String marca) {
        Marca = marca;
    }

    public String getModelo() {
        return Modelo;
    }

    public void setModelo(String modelo) {
        Modelo = modelo;
    }

    public String getCompañiaTelefonica() {
        return CompañiaTelefonica;
    }

    public void setCompañiaTelefonica(String compañiaTelefonica) {
        CompañiaTelefonica = compañiaTelefonica;
    }



}




