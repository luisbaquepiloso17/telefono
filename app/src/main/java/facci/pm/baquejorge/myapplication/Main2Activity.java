package facci.pm.baquejorge.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class Main2Activity extends AppCompatActivity {

    String object_id="";
    List<SistemaOperativo> ListaSistemaOperativo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        SistemaOperativo sistemaOperativo = new SistemaOperativo("android","2.1","1");
        sistemaOperativo.save();


        Bundle extras = getIntent().getExtras();

        object_id = extras.getString("id");

        ListaSistemaOperativo = new ArrayList<SistemaOperativo>();

        ListaSistemaOperativo = SistemaOperativo.find(
                SistemaOperativo.class, "ID_Telefono =?", object_id);


        //Rector rector1 = Rector.findById(Rector.class, Long.parseLong(object_id));

        Toast.makeText(
                this,
                ListaSistemaOperativo.get(0).getAndroid(),
                Toast.LENGTH_LONG).show();


    }
}
