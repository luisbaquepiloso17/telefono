package facci.pm.baquejorge.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements  ListView.OnItemClickListener {

    private ListView ListViewTelefonos;
    private TelefonoAdapter TelefonoAdapter;
    private List<Telefono> listaTelefonos = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListViewTelefonos = findViewById(R.id.ListViewTelefonos);
        //Click en el item
        ListViewTelefonos.setOnItemClickListener(this);

        //Ingreso de datos estáticos
        Telefono telefono = new Telefono("12345678","Alta","Lg",
                "G4","CNT") ;
        telefono.save();

        ///Se agrega mas datos estaticos.
       // Telefono telefono1 = new Telefono("12234552","Media","Samsung","G4","Claro");
        //telefono1.save();



        LlenarListaTelefonos();


        TelefonoAdapter = new TelefonoAdapter(this, listaTelefonos);
        ListViewTelefonos.setAdapter(TelefonoAdapter);


    }

    public void LlenarListaTelefonos(){

        listaTelefonos = Telefono.listAll(Telefono.class);

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

        Toast.makeText(this, String.valueOf(
                listaTelefonos.get(position).getId()), Toast.LENGTH_LONG).show();
        Intent intent = new Intent(
                MainActivity.this, Main2Activity.class);
        intent.putExtra("id", listaTelefonos.get(position).getId().toString());
        startActivity(intent);
    }
}
