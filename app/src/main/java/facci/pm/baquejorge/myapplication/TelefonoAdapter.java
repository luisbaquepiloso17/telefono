package facci.pm.baquejorge.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;

public class TelefonoAdapter extends ArrayAdapter<Telefono> {
    private Context context;
    private List<Telefono> ListaTelefonos;

    public TelefonoAdapter(Context context, List<Telefono> listaTelefonos) {
        super(context, R.layout.list_item_primera_tabla);
        this.context = context;
        this.ListaTelefonos = listaTelefonos;
    }

    @Override
    public int getCount() {
        return ListaTelefonos.size();
    }

    @Override
    public Telefono getItem(int position) {
        return  ListaTelefonos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return ListaTelefonos.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        final ViewHolder viewHolder;

        if (convertView == null || convertView.getTag() == null) {
            viewHolder = new ViewHolder();
            view = LayoutInflater.from(context).inflate(R.layout.list_item_primera_tabla, parent, false);
            viewHolder.TextViewImei = view.findViewById(R.id.TextViewImei);
            viewHolder.TextViewGama = view.findViewById(R.id.TextViewGama);
            viewHolder.TextViewMarca = view.findViewById(R.id.TextViewMarca);
            viewHolder.TextViewModelo = view.findViewById(R.id.TextViewModelo);
            viewHolder.TextViewCompañiaTelefonica = view.findViewById(R.id.TextViewCompañiaTelefonica);
            viewHolder.TextViewGama = view.findViewById(R.id.TextViewGama);

            view.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            view = convertView;
        }

        // Set text with the item name
        //viewHolder.TextViewNombre = view.findViewById(R.id.TextViewNombre);
        viewHolder.TextViewImei.setText(ListaTelefonos.get(position).getImei());
        viewHolder.TextViewGama.setText(ListaTelefonos.get(position).getGama());
        viewHolder.TextViewMarca.setText(ListaTelefonos.get(position).getMarca());
        viewHolder.TextViewModelo.setText(ListaTelefonos.get(position).getModelo());
        viewHolder.TextViewCompañiaTelefonica.setText(ListaTelefonos.get(position).getCompañiaTelefonica());
        return view;
    }

    static class ViewHolder {
        TextView TextViewImei;
        TextView TextViewGama;
        TextView TextViewMarca;
        TextView TextViewModelo;
        TextView TextViewCompañiaTelefonica;


    }
}
